import { Component, OnInit } from '@angular/core';
import { Spaceport } from '../_models/spaceport.model';
import { ActivatedRoute, Router } from '@angular/router';
import { SpaceportService } from '../services/spaceport.service';
import { first } from 'rxjs/operators';
import { Location } from '@angular/common';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';

@Component({
  selector: 'app-spaceport-edit',
  templateUrl: './spaceport-edit.component.html',
  styleUrls: ['./spaceport-edit.component.scss'],
})
export class SpaceportEditComponent implements OnInit {
  spaceportForm: FormGroup;
  spaceport: Spaceport;
  isCreateMode: boolean;
  id: number;
  loading = false;
  submitted = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private spaceportService: SpaceportService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.isCreateMode = !this.id;

    if (this.isCreateMode) {
      this.spaceport = {
        id: undefined,
        name: '',
        city: '',
        planet: '',
        IATAcode: '',
      };

      this.spaceportForm = new FormGroup({
        name: new FormControl(this.spaceport.name, [
          Validators.required,
          Validators.maxLength(90),
          Validators.minLength(4),
        ]),
        city: new FormControl(this.spaceport.city, [
          Validators.required,
          Validators.maxLength(90),
          Validators.minLength(4),
        ]),
        planet: new FormControl(this.spaceport.planet, [
          Validators.required,
          Validators.maxLength(90),
          Validators.minLength(4),
        ]),
        IATAcode: new FormControl(this.spaceport.IATAcode, [
          Validators.required,
          Validators.maxLength(4),
          Validators.minLength(3),
        ]),
      });
    }

    if (!this.isCreateMode) {
      this.getSpaceport(this.id);
    }
  }

  get f() {
    return this.spaceportForm.controls;
  }

  getSpaceport(id: number): void {
    this.spaceportService.getSpaceport(id).subscribe((spaceport) => {
      this.spaceport = spaceport;

      this.spaceportForm = new FormGroup({
        name: new FormControl(this.spaceport.name, [
          Validators.required,
          Validators.maxLength(90),
          Validators.minLength(4),
        ]),
        city: new FormControl(this.spaceport.city, [
          Validators.required,
          Validators.maxLength(90),
          Validators.minLength(4),
        ]),
        planet: new FormControl(this.spaceport.planet, [
          Validators.required,
          Validators.maxLength(90),
          Validators.minLength(4),
        ]),
        IATAcode: new FormControl(this.spaceport.IATAcode, [
          Validators.required,
          Validators.maxLength(4),
          Validators.minLength(3),
        ]),
      });
    });
  }

  onSubmit() {
    this.submitted = true;
    this.loading = true;

    if (this.spaceportForm.invalid) {
      return;
    }

    if (this.isCreateMode) {
      this.createSpaceport();
    } else {
      this.updateSpaceport();
    }
  }

  private createSpaceport() {
    this.spaceport = this.spaceportForm.value;

    this.spaceportService.addSpaceport(this.spaceport).subscribe(() => {
      this.goBack();
      this.loading = false;
    });
  }

  private updateSpaceport() {
    this.spaceport.name = this.spaceportForm.value.name;
    this.spaceport.city = this.spaceportForm.value.city;
    this.spaceport.planet = this.spaceportForm.value.planet;
    this.spaceport.IATAcode = this.spaceportForm.value.IATAcode;

    this.spaceportService.updateSpaceport(this.spaceport).subscribe(() => {
      this.goBack();
      this.loading = false;
    });
  }

  goBack(): void {
    this.router.navigate(['/spaceport/list']);
  }

  ngOnDestroy(): void {}
}
