import { Component, OnInit } from '@angular/core';
import { UseCase } from './usecase.model'

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  readonly PLAIN_USER = 'Reguliere gebruiker'
  readonly ADMIN_USER = 'Administrator'

  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'voertuig lijst',
      description: 'Hier kan een gebruiker alle voertuigen zien.',
      scenario: [
        'Gebruiker drukt op de knop vehicles',
        'De applicatie geeft een lijst met vehicle',
      ],
      actor: this.ADMIN_USER,
      precondition: 'Geen',
      postcondition: 'De actor ziet een lijst met vehicles'
    },
    {
      id: 'UC-02',
      name: 'voertuig toevoegen',
      description: 'Hiermee kan een gebruiker een voertuig toevoegen',
      scenario: ['Vanaf de lijst met voertuigen klikt de gebruiker op een toevoeg knop',
        'De gebruiker voert een formulier in en drukt op opslaan',
        'De applicatie geeft de lijst met voertuigen weer samen met de nieuw toegevoegde voertuig'],
      actor: this.ADMIN_USER,
      precondition: 'geen',
      postcondition: 'voertuig aangemaakt.'
    },
    {
      id: 'UC-03',
      name: 'voertuig aanpassen',
      description: 'Hiermee kan een gebruiker een voertuig aanpassen',
      scenario: ['Vanaf de lijst met voertuigen klikt de gebruiker op een aanpas knop',
        'De gebruiker voert een formulier in en drukt op opslaan',
        'De applicatie geeft de lijst met voertuigen weer samen met de aangepaste voertuig'],
      actor: this.ADMIN_USER,
      precondition: 'geen',
      postcondition: 'voertuig aangepast.'
    },
    {
      id: 'UC-04',
      name: 'voertuig verwijderen',
      description: 'Hiermee kan een gebruiker een voertuig verwijderen',
      scenario: ['Vanaf de lijst met voertuigen klikt de gebruiker op een verwijder knop',
        'De applicatie geeft de lijst met voertuigen weer zonder het verwijderde voertuig'],
      actor: this.ADMIN_USER,
      precondition: 'geen',
      postcondition: 'voertuig verwijderd.'
    }
    ,
    {
      id: 'UC-05',
      name: 'spaceport lijst',
      description: 'Hier kan een gebruiker alle spaceporten zien.',
      scenario: [
        'Gebruiker drukt op de knop spaceports',
        'De applicatie geeft een lijst met spaceports',
      ],
      actor: this.ADMIN_USER,
      precondition: 'Geen',
      postcondition: 'De actor ziet een lijst met spaceports'
    },
    {
      id: 'UC-06',
      name: 'spaceport toevoegen',
      description: 'Hiermee kan een gebruiker een spaceport toevoegen',
      scenario: ['Vanaf de lijst met spaceports klikt de gebruiker op een toevoeg knop',
        'De gebruiker voert een formulier in en drukt op opslaan',
        'De applicatie geeft de lijst met spaceports weer samen met de nieuw toegevoegde spaceport'],
      actor: this.ADMIN_USER,
      precondition: 'geen',
      postcondition: 'spaceport aangemaakt.'
    },
    {
      id: 'UC-07',
      name: 'spaceport aanpassen',
      description: 'Hiermee kan een gebruiker een spaceport aanpassen',
      scenario: ['Vanaf de lijst met spaceports klikt de gebruiker op een aanpas knop',
        'De gebruiker voert een formulier in en drukt op opslaan',
        'De applicatie geeft de lijst met spaceports weer samen met de aangepaste spaceport'],
      actor: this.ADMIN_USER,
      precondition: 'geen',
      postcondition: 'spaceport aangepast.'
    },
    {
      id: 'UC-08',
      name: 'spaceport verwijderen',
      description: 'Hiermee kan een gebruiker een spaceport verwijderen',
      scenario: ['Vanaf de lijst met spaceports klikt de gebruiker op een verwijder knop',
        'De applicatie geeft de lijst met spaceports weer zonder het verwijderde spaceport'],
      actor: this.ADMIN_USER,
      precondition: 'geen',
      postcondition: 'spaceport verwijderd.'
    }
    ,
    {
      id: 'UC-09',
      name: 'vlucht lijst',
      description: 'Hier kan een gebruiker alle vluchten zien.',
      scenario: [
        'Gebruiker drukt op de knop flights',
        'De applicatie geeft een lijst met vluchten',
      ],
      actor: this.ADMIN_USER,
      precondition: 'Geen',
      postcondition: 'De actor ziet een lijst met vluchten'
    },
    {
      id: 'UC-10',
      name: 'vlucht toevoegen',
      description: 'Hiermee kan een gebruiker een vlucht toevoegen',
      scenario: ['Vanaf de lijst met vluchten klikt de gebruiker op een toevoeg knop',
        'De gebruiker voert een formulier in en drukt op opslaan',
        'De applicatie geeft de lijst met vluchten weer samen met de nieuw toegevoegde vlucht'],
      actor: this.ADMIN_USER,
      precondition: 'geen',
      postcondition: 'voertuig aangemaakt.'
    },
    {
      id: 'UC-11',
      name: 'vlucht aanpassen',
      description: 'Hiermee kan een gebruiker een vlucht aanpassen',
      scenario: ['Vanaf de lijst met vluchten klikt de gebruiker op een aanpas knop',
        'De gebruiker voert een formulier in en drukt op opslaan',
        'De applicatie geeft de lijst met vluchten weer samen met de aangepaste vlucht'],
      actor: this.ADMIN_USER,
      precondition: 'geen',
      postcondition: 'vlucht aangepast.'
    },
    {
      id: 'UC-12',
      name: 'vlucht verwijderen',
      description: 'Hiermee kan een gebruiker een vlucht verwijderen',
      scenario: ['Vanaf de lijst met vluchten klikt de gebruiker op een verwijder knop',
        'De applicatie geeft de lijst met vluchten weer zonder het verwijderde vlucht'],
      actor: this.ADMIN_USER,
      precondition: 'geen',
      postcondition: 'vlucht verwijderd.'
    },
    {
      id: 'UC-13',
      name: 'inloggen',
      description: 'Hiermee kan een gebruiker zich inloggen',
      scenario: ['Vanaf het voor het eerst komen op de website beland de gebruiker op de inlog pagina',
        'met de juiste gegevens kan de gebruiker zich inloggen'],
      actor: this.ADMIN_USER,
      precondition: 'al een account hebben.',
      postcondition: 'kan naar de rest van de website navigeren.'
    },
    {
      id: 'UC-14',
      name: 'inloggen',
      description: 'Hiermee kan een gebruiker zich registreren',
      scenario: ['Vanaf het voor het eerst komen op de website beland de gebruiker op de inlog pagina',
        'als de gebruiker geen account heeft kan deze navigeren naar de registreren pagina',
        'met de juiste gegevens kan de gebruiker zich registreren'],
      actor: this.ADMIN_USER,
      precondition: 'nog geen account hebben',
      postcondition: 'de website navigeerd de gebruiker naar de login pagina.'
    }
  ]


  constructor() { }

  ngOnInit(): void {
  }

}
