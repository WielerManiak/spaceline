import { Component, OnInit } from '@angular/core';
import { Vehicle } from '../_models/vehicle.model';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { Location } from '@angular/common';
import { VehicleService } from '../services/vehicle.service';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';

@Component({
  selector: 'app-vehicle-edit',
  templateUrl: './vehicle-edit.component.html',
  styleUrls: ['./vehicle-edit.component.scss'],
})
export class VehicleEditComponent implements OnInit {
  vehicleForm: FormGroup;
  vehicle: Vehicle;
  isCreateMode: boolean;
  id: number;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private vehicleService: VehicleService
  ) {}

  ngOnInit(): void {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.isCreateMode = !this.id;

    if (this.isCreateMode) {
      this.vehicle = {
        id: undefined,
        name: '',
        seatAmount: undefined,
      };

      this.vehicleForm = new FormGroup({
        name: new FormControl(this.vehicle.name, [
          Validators.required,
          Validators.maxLength(90),
          Validators.minLength(4)
        ]),
        seatAmount: new FormControl(this.vehicle.seatAmount, [
          Validators.required,
          Validators.max(999),
          Validators.min(1)
        ]),
      });
    }

    if (!this.isCreateMode) {
      this.getVehicle(this.id);
    }
  }

  get f() {
    return this.vehicleForm.controls;
  }

  getVehicle(id: number): void {
    this.vehicleService.getVehicle(id).subscribe((vehicle) => {
      this.vehicle = vehicle;

      this.vehicleForm = new FormGroup({
        name: new FormControl(this.vehicle.name, [
          Validators.required,
          Validators.maxLength(90),
          Validators.minLength(4)
        ]),
        seatAmount: new FormControl(this.vehicle.seatAmount, [
          Validators.required,
          Validators.max(999),
          Validators.min(1)
        ]),
      });
    });
  }

  onSubmit() {
    this.submitted = true;
    this.loading = true;

    if (this.vehicleForm.invalid) {
      return;
    }

    if (this.isCreateMode) {
      this.createVehicle();
    } else {
      this.updateVehicle();
    }
  }

  private createVehicle() {
    this.vehicle = this.vehicleForm.value;

    this.vehicleService.addVehicle(this.vehicle).subscribe(() => {
      this.goBack();
      this.loading = false;
    });
  }

  private updateVehicle() {
    this.vehicle.name = this.vehicleForm.value.name;
    this.vehicle.seatAmount = this.vehicleForm.value.seatAmount;

    this.vehicleService.updateVehicle(this.vehicle).subscribe(() => {
      this.goBack();
      this.loading = false;
    });
  }

  goBack(): void {
    this.router.navigate(['/vehicle/list']);
  }
}
