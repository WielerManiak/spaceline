import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { VehicleEditComponent } from './vehicle-edit.component';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { VehicleService } from '../services/vehicle.service';
import { Vehicle } from '../_models/vehicle.model';


describe('VehicleEditComponent', () => {
  let component: VehicleEditComponent;
  let fixture: ComponentFixture<VehicleEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [ VehicleEditComponent ],
      providers: [FormBuilder, VehicleService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add vehicle', () => {
    component.vehicle = {
      id: undefined,
      name: '',
      seatAmount: 1,
    };
    component.isCreateMode = true;
    component.vehicleForm = new FormGroup({
      name: new FormControl(component.vehicle.name, [
        Validators.required,
        Validators.maxLength(90),
        Validators.minLength(4)
      ]),
      seatAmount: new FormControl(component.vehicle.seatAmount, [
        Validators.required,
        Validators.max(999),
        Validators.min(1)
      ]),
    });

    component.onSubmit()

    expect(component.onSubmit).toThrowError();
    //idk how to test this component
  })
});
