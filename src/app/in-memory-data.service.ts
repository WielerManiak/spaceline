import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Flight } from './_models/flight.model';
import { Spaceport } from './_models/spaceport.model';
import { Vehicle } from './_models/vehicle.model';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  createDb() {
    const spaceports = [
      { id: 1,
        name: "Kennedy Space Center",
        city: "Merritt island",
        planet: "Earth",
        IATAcode: "KSC"
      },
      {
        id: 2,
        name: "South Texas Launch Site",
        city: "Boca Chika",
        planet: "Earth",
        IATAcode: "STLS"

      },
      {
        id: 3,
        name: "Mars Space Center",
        city: "Lowell",
        planet: "Mars",
        IATAcode: "MSC"
      }
    ];
    const vehicles = [
      {
        id: 1,
        name: 'Starship',
        seatAmount: 8,
      },
      {
        id: 2,
        name: 'Falcon 9 Crew Dragon',
        seatAmount: 4,
      },
      {
        id: 3,
        name: 'Moonship',
        seatAmount: 6,
      }
    ]
    const flights = [
      {
        id: 1,
        name: 'KSC -> MSC',
        arivalId: spaceports[0].id,
        departureId: spaceports[2].id,
        vehicleId: vehicles[0].id,
        date: new Date()
      }
    ]
    return {spaceports, vehicles, flights};
  }

  // genId(spaceport: Spaceport[]): number {
  //   return spaceport.length > 0 ? Math.max(...spaceport.map(spaceport => spaceport.id)) + 1 : 11;
  // }

  genId<T extends Spaceport | Vehicle | Flight>(myTable: T[]): number {
    return myTable.length > 0 ? Math.max(...myTable.map(t => t.id)) + 1 : 11;
  }

  constructor() { }
}
