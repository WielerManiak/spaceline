import { Component, OnInit } from '@angular/core';
import { FlightService } from '../services/flight.service';
import { SpaceportService } from '../services/spaceport.service';
import { VehicleService } from '../services/vehicle.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Vehicle } from '../_models/vehicle.model';
import { Flight } from '../_models/flight.model';
import { Spaceport } from '../_models/spaceport.model';

@Component({
  selector: 'app-flight-details',
  templateUrl: './flight-details.component.html',
  styleUrls: ['./flight-details.component.scss'],
})
export class FlightDetailsComponent implements OnInit {
  id: number;
  loading = false;
  flight: Flight;
  departurePort: Spaceport;
  arivalPort: Spaceport;
  vehicle: Vehicle;

  constructor(
    private flightService: FlightService,
    private vehicleService: VehicleService,
    private spaceportService: SpaceportService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.getFlight(this.id);
  }

  getFlight(id: number): void {
    this.flightService
      .getFlight(id)
      .subscribe((flight) => {
        this.flight = flight;
        console.log(this.flight)
        this.spaceportService.getSpaceport(this.flight.departureId).subscribe((departurePort) => {
          this.departurePort = departurePort;
        })

        this.spaceportService.getSpaceport(this.flight.arivalId).subscribe((arivalport) => {
          this.arivalPort = arivalport;
        })

        this.vehicleService.getVehicle(this.flight.vehicleId).subscribe((vehicle) => {
          this.vehicle = vehicle;
        })

      });
  }

  onDelete(id: number): void {
    this.loading = true;
    this.flightService.deleteFlight(id)
    .subscribe(() => {
      this.router.navigate(['/flight/list'])
      this.loading= false;
    })
  }
}
