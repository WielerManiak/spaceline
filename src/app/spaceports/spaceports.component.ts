import { Component, OnInit } from '@angular/core';
import { Spaceport } from '../_models/spaceport.model';
import { SpaceportService } from '../services/spaceport.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-spaceports',
  templateUrl: './spaceports.component.html',
  styleUrls: ['./spaceports.component.scss'],
})
export class SpaceportsComponent implements OnInit {
  spaceports: Spaceport[];
  loading = false;

  constructor(
    private spaceportService: SpaceportService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.getSpaceports();
  }

  getSpaceports(): void {
    this.spaceportService
      .getSpaceports()
      .subscribe((spaceports) => (this.spaceports = spaceports));
  }

  onDelete(id: number): void {
    this.loading = true;
    this.spaceportService.deleteSpaceport(id).subscribe(() => {
      let index = this.spaceports.findIndex((s) => s.id === id);
      this.spaceports.splice(index, 1);
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate(['/spaceport/list']);
      this.loading = false;
    });
  }

  displayedColumns: string[] = ['id', 'name', 'city', 'planet', 'IATAcode', 'actions'];
}
