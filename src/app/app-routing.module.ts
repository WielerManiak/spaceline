import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component'
import { VehiclesComponent } from './vehicles/vehicles.component'
import { SpaceportsComponent } from './spaceports/spaceports.component'
import { FlightsComponent } from './flights/flights.component'
import { AboutComponent } from './about/about.component'
import { SigninComponent } from './auth/signin/signin.component'
import { SignupComponent } from './auth/signup/signup.component'
import { SpaceportDetailComponent } from './spaceport-detail/spaceport-detail.component'
import { SpaceportEditComponent } from './spaceport-edit/spaceport-edit.component'
import { PageNotFoundComponent } from './page-not-found/page-not-found.component'
import { VehicleEditComponent } from './vehicle-edit/vehicle-edit.component'
import { FlightEditComponent } from './flight-edit/flight-edit.component'
import { FlightDetailsComponent } from './flight-details/flight-details.component'
import { AuthGuard } from './_helpers/auth.guard';
import { Role } from './_models/role'

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'vehicle/list', component: VehiclesComponent, canActivate: [AuthGuard], data: { roles: [Role.Admin] }},
  { path: 'vehicle/update/:id', component: VehicleEditComponent, canActivate: [AuthGuard], data: { roles: [Role.Admin] } },
  { path: 'vehicle/create', component: VehicleEditComponent, canActivate: [AuthGuard], data: { roles: [Role.Admin] } },
  { path: 'spaceport/list', component: SpaceportsComponent, canActivate: [AuthGuard], data: { roles: [Role.Admin] } },
  { path: 'spaceport/details/:id', component: SpaceportDetailComponent, canActivate: [AuthGuard], data: { roles: [Role.Admin] } },
  { path: 'spaceport/create', component: SpaceportEditComponent, canActivate: [AuthGuard], data: { roles: [Role.Admin] } },
  { path: 'spaceport/update/:id', component: SpaceportEditComponent, canActivate: [AuthGuard], data: { roles: [Role.Admin] } },
  { path: 'flight/list', component: FlightsComponent, canActivate: [AuthGuard], data: { roles: [Role.Admin] } },
  { path: 'flight/create', component: FlightEditComponent, canActivate: [AuthGuard], data: { roles: [Role.Admin] } },
  { path: 'flight/update/:id', component: FlightEditComponent, canActivate: [AuthGuard], data: { roles: [Role.Admin] } },
  { path: 'flight/details/:id', component: FlightDetailsComponent, canActivate: [AuthGuard], data: { roles: [Role.Admin] }},
  { path: 'about', component: AboutComponent },
  { path: 'signin', component: SigninComponent },
  { path: 'register', component: SignupComponent },
  { path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
