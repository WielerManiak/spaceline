import { Component, OnInit } from '@angular/core';
import { User } from '../_models/user';
import { Role } from '../_models/role';
import { AuthenticationService } from '../services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  currentUser: User;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  get isAdmin() {
    return this.currentUser && this.currentUser.role === Role.Admin;
}

  ngOnInit(): void {}

  logout() {
    this.authenticationService.logout();
    this.currentUser = undefined;
    this.router.navigate(['/signin']);
  }
}
