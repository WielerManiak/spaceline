import { Component, OnInit } from '@angular/core';
import { Vehicle } from '../_models/vehicle.model'
import { VehicleService } from '../services/vehicle.service'
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.scss']
})
export class VehiclesComponent implements OnInit {
  vehicles: Vehicle[];
  loading = false;

  constructor(
    private vehicleService: VehicleService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getVehicles();
  }

  getVehicles(): void {
    this.vehicleService
    .getVehicles()
    .subscribe((vehicles) => (this.vehicles = vehicles));
  }

  onDelete(id: number): void {
    this.loading = true;
    this.vehicleService.deleteVehicle(id).subscribe(() => {
      let index = this.vehicles.findIndex((v) => v.id === id);
      this.vehicles.splice(index, 1);
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate(['/vehicle/list'])
      this.loading = false;
    })
  }

  displayedColumns: string[] = ['id', 'name', 'seatAmount', 'actions'];

}
