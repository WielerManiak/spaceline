import { Component, OnInit } from '@angular/core';
import { Spaceport } from '../_models/spaceport.model';
import { ActivatedRoute } from '@angular/router';
import { SpaceportService} from '../services/spaceport.service'

@Component({
  selector: 'app-spaceport-detail',
  templateUrl: './spaceport-detail.component.html',
  styleUrls: ['./spaceport-detail.component.scss']
})
export class SpaceportDetailComponent implements OnInit {

  spaceport: Spaceport;

  constructor(private route: ActivatedRoute, private spaceportService: SpaceportService) { }

  ngOnInit(): void {
    this.getSpaceport();
  }

  getSpaceport(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.spaceportService.getSpaceport(id)
      .subscribe(spaceport => this.spaceport = spaceport)
  }

  ngOnDestroy(): void {
  }

}
