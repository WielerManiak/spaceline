import { Injectable } from '@angular/core';
import { Spaceport } from '../_models/spaceport.model';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { MessageService } from '../message.service';

@Injectable({
  providedIn: 'root',
})
export class SpaceportService {
  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}

  private spaceportsUrl = 'api/spaceports';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  getSpaceports(): Observable<Spaceport[]> {
    return this.http.get<Spaceport[]>(this.spaceportsUrl).pipe(
      tap((_) => this.log('fetched spaceports')),
      catchError(this.handleError<Spaceport[]>('getSpaceports', []))
    );
  }

  getSpaceport(id: number): Observable<Spaceport> {
    const url = `${this.spaceportsUrl}/${id}`;

    return this.http.get<Spaceport>(url).pipe(
      tap((_) => this.log(`fetched spaceport id=${id}`)),
      catchError(this.handleError<Spaceport>(`getSpaceport id=${id}`))
    );
  }

  updateSpaceport(spaceport: Spaceport): Observable<any> {
    return this.http.put(this.spaceportsUrl, spaceport, this.httpOptions).pipe(
      tap((_) => this.log(`updated spaceport id=${spaceport.id}`)),
      catchError(this.handleError<any>('updateSpaceport'))
    );
  }

  addSpaceport(spaceport: Spaceport): Observable<any> {
    return this.http
      .post<Spaceport>(this.spaceportsUrl, spaceport, this.httpOptions)
      .pipe(
        tap((newSpaceport: Spaceport) =>
          this.log(`added spaceport id=${newSpaceport.id}`)
        ),
        catchError(this.handleError<Spaceport>('addSpaceport'))
      );
  }

  deleteSpaceport(spaceport: Spaceport | number): Observable<Spaceport> {
    const id = typeof spaceport === 'number' ? spaceport : spaceport.id;
    const url = `${this.spaceportsUrl}/${id}`;

    return this.http.delete<Spaceport>(url, this.httpOptions).pipe(
      tap((_) => this.log(`deleted spaceport id=${id}`)),
      catchError(this.handleError<Spaceport>('deleteSpaceport'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: string) {
    this.messageService.add(`SpaceportService: ${message}`);
  }
}
