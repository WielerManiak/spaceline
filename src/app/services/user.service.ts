import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models/user';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`api/users`);
    }

    register(user: User) {
        return this.http.post(`api/users/register`, user);
    }

    getById(id: number) {
      return this.http.get<User>(`api/users/${id}`);
  }

    delete(id: number) {
        return this.http.delete(`api/users/${id}`);
    }
}
