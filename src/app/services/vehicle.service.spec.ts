import { HttpClientTestingModule } from '@angular/common/http/testing';

// Other imports
import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Vehicle } from '../_models/vehicle.model';
import { VehicleService } from './vehicle.service';
import { doesNotReject } from 'assert';

describe('VehicleService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [VehicleService],
    })
  );

  it('should create', () => {
    const service: VehicleService = TestBed.get(VehicleService);
    expect(service).toBeTruthy();
  });

  it('should have function get all vehicles', () => {
    const service: VehicleService = TestBed.get(VehicleService);
    expect(service.getVehicles).toBeTruthy();
  });

  it('should have function get a vehicle', () => {
    const service: VehicleService = TestBed.get(VehicleService);
    expect(service.getVehicle).toBeTruthy();
  });

  it('should have function add a vehicle', () => {
    const service: VehicleService = TestBed.get(VehicleService);
    expect(service.addVehicle).toBeTruthy();
  });

  it('should have function delete a vehicle', () => {
    const service: VehicleService = TestBed.get(VehicleService);
    expect(service.deleteVehicle).toBeTruthy();
  });

  it('should have function update a vehicle', () => {
    const service: VehicleService = TestBed.get(VehicleService);
    expect(service.updateVehicle).toBeTruthy();
  });

  it('should return a list', () => {
    const service: VehicleService = TestBed.get(VehicleService);

    let vehicles: Vehicle[];

    service.getVehicles().subscribe((v) => (vehicles = v) => {
      expect(vehicles).toBeGreaterThan(0);
      expect(vehicles).toHaveSize(3);
    });
  });

  it('should add a vehicle', () => {
    const service: VehicleService = TestBed.get(VehicleService);
    let v = {
      id: undefined,
      name: 'testship',
      seatAmount: 1,
    };
    service.addVehicle(v).subscribe(() => {
      service.getVehicles().subscribe((v) => (vehicles = v) => {
        expect(vehicles).toBeGreaterThan(0);
        expect(vehicles).toHaveSize(4);
        expect(vehicles[4].name).toEqual('testship');
      });
    });
  });

  it('should delete a vehicle', () => {
    const service: VehicleService = TestBed.get(VehicleService);

    service.deleteVehicle(1).subscribe(() => {
      service.getVehicles().subscribe((v) => (vehicles = v) => {
        expect(vehicles).toHaveSize(3);
        expect(vehicles[0].id).toEqual(2);
      });
    });
  });

  it('should update a vehicle', () => {
    const service: VehicleService = TestBed.get(VehicleService);
    let v = {
      id: 2,
      name: 'testship',
      seatAmount: 1,
    };
    service.updateVehicle(v).subscribe(() => {
      service.getVehicles().subscribe((v) => (vehicles = v) => {
        expect(vehicles).toHaveSize(3);
        expect(vehicles[0].id).toEqual(2);
        expect(vehicles[0].name).toEqual('testship');
        expect(vehicles[0].seatAmount).toEqual(1);
      });
    });
  });
});
