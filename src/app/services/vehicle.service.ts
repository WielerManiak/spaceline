import { Injectable } from '@angular/core';
import { Vehicle } from '../_models/vehicle.model';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { MessageService } from '../message.service';

@Injectable({
  providedIn: 'root',
})
export class VehicleService {
  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}

  private vehiclesUrl = 'api/vehicles';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  getVehicles(): Observable<Vehicle[]> {
    return this.http.get<Vehicle[]>(this.vehiclesUrl).pipe(
      tap((_) => this.log('fetched vehicles')),
      catchError(this.handleError<Vehicle[]>('getVehicles', []))
    );
  }

  getVehicle(id: number): Observable<Vehicle> {
    const url = `${this.vehiclesUrl}/${id}`;

    return this.http.get<Vehicle>(url).pipe(
      tap((_) => this.log(`fetched vehicle id=${id}`)),
      catchError(this.handleError<any>(`getVehicle id=${id}`))
    );
  }

  updateVehicle(vehicle: Vehicle): Observable<any> {
    return this.http.put(this.vehiclesUrl, vehicle, this.httpOptions).pipe(
      tap((_) => this.log(`updated vehicle id=${vehicle.id}`)),
      catchError(this.handleError<any>('updateVehicle'))
    );
  }

  addVehicle(vehicle: Vehicle): Observable<any> {
    return this.http
      .post<Vehicle>(this.vehiclesUrl, vehicle, this.httpOptions)
      .pipe(
        tap((newVehicle: Vehicle) =>
          this.log(`added vehicle id=${newVehicle.id}`)
        ),
        catchError(this.handleError<Vehicle>('addVehicle'))
      );
  }

  deleteVehicle(vehicle: Vehicle | number): Observable<Vehicle> {
    const id = typeof vehicle === 'number' ? vehicle : vehicle.id;
    const url = `${this.vehiclesUrl}/${id}`;

    return this.http.delete<Vehicle>(url, this.httpOptions).pipe(
      tap((_) => this.log(`deleted vehicle id=${id}`)),
      catchError(this.handleError<Vehicle>('deleteVehicle'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: string) {
    this.messageService.add(`VehicleService: ${message}`);
  }
}
