import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SpaceportService } from './spaceport.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('SpaceportService', () => {
  let service: SpaceportService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(SpaceportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
