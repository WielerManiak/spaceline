import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MessageService } from '../message.service';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Flight } from '../_models/flight.model';

@Injectable({
  providedIn: 'root',
})
export class FlightService {
  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}

  private flightsURL = 'api/flights';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  getFlights(): Observable<Flight[]> {
    return this.http.get<Flight[]>(this.flightsURL).pipe(
      tap((_) => this.log('fetched flights')),
      catchError(this.handleError<Flight[]>('getFlights', []))
    );
  }

  getFlight(id: number): Observable<Flight> {
    const url = `${this.flightsURL}/${id}`;

    return this.http.get<Flight>(url).pipe(
      tap((_) => this.log(`fetched flight id=${id}`)),
      catchError(this.handleError<Flight>(`getFlight id=${id}`))
    );
  }

  updateFlight(flight: Flight): Observable<any> {
    return this.http.put(this.flightsURL, flight, this.httpOptions).pipe(
      tap((_) => this.log(`updated flight id=${flight.id}`)),
      catchError(this.handleError<any>('updateFlight'))
    );
  }

  addFlight(flight: Flight): Observable<any> {
    return this.http
      .post<Flight>(this.flightsURL, flight, this.httpOptions)
      .pipe(
        tap((newFlight: Flight) => this.log(`added flight id=${newFlight.id}`)),
        catchError(this.handleError<Flight>('addFlight'))
      );
  }

  deleteFlight(flight: Flight | number): Observable<any> {
    const id = typeof flight === 'number' ? flight : flight.id;
    const url = `${this.flightsURL}/${id}`;

    return this.http.delete<Flight>(url, this.httpOptions).pipe(
      tap((_) => this.log(`deleted flight id=${id}`)),
      catchError(this.handleError<Flight>('deleteFlight'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: string) {
    this.messageService.add(`FlightService: ${message}`);
  }
}
