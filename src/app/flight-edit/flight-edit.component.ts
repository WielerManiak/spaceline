import { Component, OnInit } from '@angular/core';
import { Spaceport } from '../_models/spaceport.model';
import { SpaceportService } from '../services/spaceport.service';
import { VehicleService } from '../services/vehicle.service'
import { FlightService } from '../services/flight.service'
import { ActivatedRoute, Router } from '@angular/router';
import { Vehicle } from '../_models/vehicle.model';
import { Flight } from '../_models/flight.model';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from "@angular/forms";

@Component({
  selector: 'app-flight-edit',
  templateUrl: './flight-edit.component.html',
  styleUrls: ['./flight-edit.component.scss']
})
export class FlightEditComponent implements OnInit {
  spaceports: Spaceport[];
  vehicles: Vehicle[];
  isCreateMode: boolean;
  id: number;
  loading = false;
  submitted = false;
  flight: Flight;
  selectedDeparture: Spaceport;
  flightForm: FormGroup;

  constructor(
    private spaceportService: SpaceportService,
    private vehicleService: VehicleService,
    private flightService: FlightService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder

  ) { }


  ngOnInit(): void {
    this.getSpaceports();
    this.getVehicles();

    this.id = +this.route.snapshot.paramMap.get('id');
    this.isCreateMode = !this.id;

    if(this.isCreateMode){
      this.flight = {
        id: undefined,
        name: '',
        departureId: undefined,
        arivalId: undefined,
        vehicleId: undefined,
        date: undefined
      };

      this.flightForm = new FormGroup({
        departureId: new FormControl(this.flight.departureId, [
          Validators.required
        ]),
        arivalId: new FormControl(this.flight.arivalId, [
          Validators.required
        ]),
        vehicleId: new FormControl(this.flight.vehicleId, [
          Validators.required
        ]),
        date: new FormControl(this.flight.date, [
          Validators.required
        ])
      })

    }
  }

  get f() {
    return this.flightForm.controls;
  }

  getFlight(id: number): void{
      this.flightService
      .getFlight(id)
      .subscribe((flight) => (this.flight = flight) )
  }

  getSpaceports(): void {
    this.spaceportService
      .getSpaceports()
      .subscribe((spaceports) => (this.spaceports = spaceports));
  }

  getVehicles(): void {
    this.vehicleService
    .getVehicles()
    .subscribe((vehicles) => (this.vehicles = vehicles));
  }

  onSubmit() {
    this.submitted = true;
    this.loading = true;
    if(this.isCreateMode){
      this.createFlight();
    }
  }

  private createFlight() {
    this.flight = this.flightForm.value;

    const spaceportDeparture = this.spaceports.find(spaceport => spaceport.id === this.flight.departureId);
    const spaceportArival = this.spaceports.find(spaceport => spaceport.id === this.flight.arivalId)

    this.flight.name = `${spaceportDeparture.IATAcode} -> ${spaceportArival.IATAcode}`

    this.flightService
      .addFlight(this.flight)
      .subscribe(() => {
        this.goBack();
        this.loading = false;
      });
  }

  goBack(): void {
    this.router.navigate(['/flight/list']);
  }

  ngOnDestroy(): void {}
}

