import { Component, OnInit } from '@angular/core';
import { Flight } from '../_models/flight.model';
import { FlightService } from '../services/flight.service';
import { Spaceport } from '../_models/spaceport.model';
import { Vehicle } from '../_models/vehicle.model';

@Component({
  selector: 'app-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.scss'],
})
export class FlightsComponent implements OnInit {
  flights: Flight[];

  constructor(private flightService: FlightService) {}

  ngOnInit(): void {
    this.getFlights();
  }

  getFlights(): void {
    this.flightService
      .getFlights()
      .subscribe((flights) => (this.flights = flights));
  }
  displayedColumns: string[] = ['id', 'name', 'date', 'actions']
}
