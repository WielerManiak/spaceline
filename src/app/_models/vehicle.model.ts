export interface Vehicle {
  id: number;
  name: string;
  seatAmount: number;
}
