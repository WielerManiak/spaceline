import { Spaceport } from './spaceport.model';
import { Vehicle } from './vehicle.model';

export interface Flight {
  id: number;
  name: string;
  departureId: number;
  arivalId: number;
  vehicleId: number;
  date: Date;
}
