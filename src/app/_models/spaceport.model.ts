export interface Spaceport {
  id: number;
  name: string;
  city: string;
  planet: string;
  IATAcode: string;
}
