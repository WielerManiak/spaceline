import { Spaceport } from './_models/spaceport.model';
import { Vehicle } from './_models/vehicle.model';

export const SPACEPORTS: Spaceport[] = [
  { id: 1,
    name: "Kennedy Space Center",
    city: "Merritt island",
    planet: "Earth",
    IATAcode: "KSC"
  },
  {
    id: 2,
    name: "South Texas Launch Site",
    city: "Boca Chika",
    planet: "Earth",
    IATAcode: "STLS"

  },
  {
    id: 3,
    name: "Mars Space Center",
    city: "Lowell",
    planet: "Mars",
    IATAcode: "MSC"
  }
];

export const VEHICLES: Vehicle[] = [
  {
    id: 1,
    name: 'Starship',
    seatAmount: 8,
  },
  {
    id: 2,
    name: 'Falcon 9 Crew Dragon',
    seatAmount: 4,
  },
  {
    id: 1,
    name: 'Moonship',
    seatAmount: 6,
  }
];
